import yaml

with open("../inventory.yaml", 'r') as inv:
    with open('../zakupy.txt', 'a') as z:

        try:
            inventory = yaml.safe_load(inv)
            ryjce = int(inventory['ryjce'])
            z.write('na ' + str(ryjce) + ' potrzeba: \n')
            zakupy = dict(inventory['ryby']['zakupy'])
            for groups in zakupy:

                for item in zakupy[groups]:
                    # print(item)
                    for name, quant in item.items():
                        z.write(name + ' ' + str(float(quant) * int(ryjce)) + '\n')
        except yaml.YAMLError as exc:
            print(exc)
